<?php

declare(strict_types=1);

namespace App\Generator;


use App\Repository\LinkRepository;

final class CodeGenerator
{
    private $linkRepository;

    public function __construct(LinkRepository $linkRepository)
    {
        $this->linkRepository = $linkRepository;
    }

    public function generate()
    {
        do {
            $hash = bin2hex(random_bytes(20));
            $code = strtoupper(substr($hash, 0, 4));
        } while ($this->isUsedCode($code));

        return $code;
    }

    private function isUsedCode(string $code)
    {
        return null !== $this->linkRepository->findOneBy(['code' => $code]);
    }
}
