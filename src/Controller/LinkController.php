<?php

namespace App\Controller;

use App\Context\UserContext;
use App\Entity\Link;
use App\Form\LinkCreateType;
use App\Generator\CodeGenerator;
use App\Repository\LinkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LinkController extends AbstractController
{
    private $userContext;
    private $linkRepository;

    public function __construct(UserContext $userContext, LinkRepository $linkRepository)
    {
        $this->userContext = $userContext;
        $this->linkRepository = $linkRepository;
    }

    public function create(Request $request, CodeGenerator $codeGenerator)
    {
        $link = new Link();
        $form = $this->createForm(LinkCreateType::class, $link);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Link $link */
            $link = $form->getData();
            $link->setUser($this->userContext->getUser());
            $link->setCode($codeGenerator->generate());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return new JsonResponse(['message' => 'successfully']);
        }

        return new JsonResponse($form->getErrors());
    }

    public function index(LinkRepository $linkRepository)
    {
        $result = [];
        $entities = $linkRepository->getUserLinks($this->userContext->getUser());
        foreach ($entities as $entity) {
            $result[] = $entity->serialize();
        }

        return new JsonResponse($result);
    }

    public function transfer(string $code)
    {
        $link = $this->linkRepository->findOneBy(['code' => $code]);
        if ($link === null) {
            throw new NotFoundHttpException('Link not found');
        }
        $link->setHits($link->getHits() + 1);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new RedirectResponse($link->getUrl());
    }
}
