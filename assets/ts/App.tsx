import * as React from "react";
import {useState} from "react";
import Modal from '../../node_modules/react-bootstrap/esm/Modal';
import Button from 'react-bootstrap/Button';
import {Container, Form, Nav, Navbar, Table} from "../../node_modules/react-bootstrap/esm/index";

class Row {
    url: string;
    id: number;
    name: string;
    hits: number|null;
    code: string;
}

interface IProps {
    isLoggedIn: boolean
}

interface IState {
    rows: Row[];
    url: string;
    name: string;
    hits: number|null;
    code: string;
}

export class App extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = { rows: [], name: '', url: '', hits: null, code: ''}
    }

    async componentDidMount() {
        await fetch('/links').then((response) => {
            return response.json();
        }).then((data) => {
            let rows = [];
            for (const el of data) {
                let row = new Row();
                row.id = el['id'];
                row.url = el['url'];
                row.hits = el['hits'];
                row.name = el['name'];
                row.code = el['code'];
                rows.push(row);

            }
            this.setState({rows: rows});
        })
    }


    CreateModal() {
        const [show, setShow] = useState(false);
        const [url, setUrl] = useState('');
        const [name, setName] = useState('');

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);
        const handleSave = async () => {
            await fetch('/links/create', {
                method: 'POST',
                body: JSON.stringify({
                    name: name,
                    url: url
                })
            })
        };

        return (
            <>
                <Button variant="primary" onClick={handleShow}>
                    Create a link
                </Button>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="formUrl">
                            <Form.Label>URL</Form.Label>
                            <Form.Control type="text" placeholder="URL" value={url} onChange={e => setUrl(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="formName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Name" value={name} onChange={e => setName(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleSave}>
                            Create
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }

    LoginLogoutLinks() {
        if (window.isLoggedIn) {
            return <a href={"/logout"}>Logout</a>
        }

        return <>
            <Nav.Link href={"/login"}>Login</Nav.Link>
            <Nav.Link href={"/register"}>Register as user</Nav.Link>
            <Nav.Link href={"/register-admin/register"}>Register as admin</Nav.Link>
        </>
    }

    render() {
        return (
            <div>
                <Navbar bg="dark" expand="lg" variant="dark">
                    <Navbar.Brand href="#home">Links</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        </Nav>
                        <Form inline>
                            <this.LoginLogoutLinks/>
                        </Form>
                    </Navbar.Collapse>
                </Navbar>
                <Container>
                    { this.props.isLoggedIn ? <this.CreateModal/> : null }
                    <Table striped bordered hover>
                        <tbody>
                        {this.state.rows.map((row, i) => {
                            return <tr key={row.id}>
                                <td>{row.name}</td>
                                <td>{row.url}</td>
                                <td>{row.hits}</td>
                                <td>{row.code}</td>
                            </tr>
                        })}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}
