/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
import * as React from 'react';
import {render} from 'react-dom';
// any CSS you import will output into a single css file (app.css in this case)
import {App} from './App';
import './app.scss';

declare global {
    interface Window {
        isLoggedIn: boolean
    }
}

render(<App isLoggedIn={window.isLoggedIn}/>, document.getElementById('root'));
